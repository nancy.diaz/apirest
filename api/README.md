# Api Rest
 

_1. Debe tener instalado maven_
_2. Descargar el archivo y descomprimir_
_3. Crear o importar la base de datos que se encuentra en_ 

```
src/main/resources data.sql
```
_4. Debe hacer los ajustes en el archivo_

```
src/main/resources.application.properties
```

_tanto en_

```
spring.datasource.username

```
_y_
```
spring.datasource.password

```
_para acceder a la base de datos mysql con sus credenciales_

_5. Verifique que se tenga la misma version de java en el archivo_

```
pom.xml

``` 
_en_
```
<properties>
   <java.version>1.8</java.version>
</properties>

```

_6. En consola ejecute_ 
```
mvn install

``` 

_7. Luego_
```
mvn spring-boot:run

```

_8. Finalmente Para probar la aplicación ingrese:_
```
http://localhost:8080/api/v1/users

```

_Para mostrar la lista de usuarios utilice:_ 
```
GET http://localhost:8080/api/v1/users
```

_Para invocar un solo usuario utilice:_ 
```
GET http://localhost:8080/api/v1/users/{id}
```

_Para crear un nuevo usuario utilice:_ 
```
POST http://localhost:8080/api/v1/users
```

_Para modificar un usuario utilice:_ 
```
PUT http://localhost:8080/api/v1/users/{id}
```

_Para eliminar un usuario utilice:_ 
```
DELETE http://localhost:8080/api/v1/users/{id}
```